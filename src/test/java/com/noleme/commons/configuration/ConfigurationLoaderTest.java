package com.noleme.commons.configuration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 13/04/2020
 */
public class ConfigurationLoaderTest
{
    @Test
    void propertiesLoadTest()
    {
        Properties props = new Properties();
        props.setProperty("a", "string");
        props.setProperty("b", "\"encased-string\"");
        props.setProperty("c", "\"not-really-encased-string");
        props.setProperty("d", "1234");
        props.setProperty("e", "12.34");
        props.setProperty("f", "true");
        props.setProperty("g", "false");

        Configuration conf = ConfigurationLoader.load(props, new Configuration());

        Assertions.assertEquals("string", conf.getString("a"));
        Assertions.assertEquals("encased-string", conf.getString("b"));
        Assertions.assertEquals("\"not-really-encased-string", conf.getString("c"));
        Assertions.assertEquals(1234, conf.getInteger("d"));
        Assertions.assertEquals(12.34, conf.get("e"));
        Assertions.assertEquals(true, conf.get("f"));
        Assertions.assertEquals(false, conf.get("g"));
    }

    private class Configuration extends com.noleme.commons.configuration.Configuration<Configuration> {}
}
