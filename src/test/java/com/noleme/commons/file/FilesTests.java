package com.noleme.commons.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/12/2019
 */
public class FilesTests
{
    @Test
    void fileExists()
    {
        boolean exists = Files.fileExists("src/test/resources/com/noleme/commons/file/collection.txt");

        Assertions.assertEquals(true, exists);
    }

    @Test
    void fileDoesNotExists()
    {
        boolean exists = Files.fileExists("src/test/resources/com/noleme/commons/file/no-collection.txt");

        Assertions.assertEquals(false, exists);
    }

    @Test
    void fileReadFrom() throws IOException
    {
        String text = Files.readFrom("src/test/resources/com/noleme/commons/file/test.txt");

        Assertions.assertEquals("abcde", text);
    }
}
