package com.noleme.commons.file;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/12/2019
 */
public class ResourcesTests
{
    @Test
    void resourceExists()
    {
        boolean exists = Resources.exists("com/noleme/commons/file/collection.txt");

        Assertions.assertEquals(true, exists);
    }

    @Test
    void resourceDoesNotExists()
    {
        boolean exists = Resources.exists("com/noleme/commons/file/no-collection.txt");

        Assertions.assertEquals(false, exists);
    }

    @Test
    void resourceReadFrom() throws IOException
    {
        String text = Resources.readFrom("com/noleme/commons/file/test.txt");

        Assertions.assertEquals("abcde", text);
    }
}
