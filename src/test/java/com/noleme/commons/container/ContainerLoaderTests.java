package com.noleme.commons.container;

import com.noleme.commons.file.Resources;
import com.noleme.commons.file.loader.CollectionHandler;
import com.noleme.commons.file.loader.ContainerLoader;
import com.noleme.commons.file.loader.MapHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/12/2019
 */
public class ContainerLoaderTests
{
    private static final List<String> expectedCollection = Lists.of(
        "some", "collection", "of", "strings", "and", "strings", "yay"
    );
    private static final Map<String, String> expectedMap = Maps.of(
        "with", "values",
        "of", "all",
        "kinds", "and",
        "some", null,
        "not", "quite",
        "standard", "entries"
    );

    @Test
    void loadCollectionFromFile() throws IOException
    {
        List<String> col = ContainerLoader.load("src/test/resources/com/noleme/commons/file/collection.txt", new ArrayList<>(), new CollectionHandler<>());

        Assertions.assertEquals(expectedCollection, col);
    }

    @Test
    void loadCollectionFromResource() throws IOException
    {
        List<String> col = ContainerLoader.load("com/noleme/commons/file/collection.txt", new ArrayList<>(), new CollectionHandler<>());

        Assertions.assertEquals(expectedCollection, col);
    }

    @Test
    void loadCollectionFromStream() throws IOException
    {
        List<String> col = ContainerLoader.load("com/noleme/commons/file/collection.txt", Resources::streamFrom, new ArrayList<>(), new CollectionHandler<>());

        Assertions.assertEquals(expectedCollection, col);
    }

    @Test
    void loadMapFromFile() throws IOException
    {
        Map<String, String> map = ContainerLoader.load("src/test/resources/com/noleme/commons/file/map.txt", new HashMap<>(), new MapHandler("="));

        Assertions.assertEquals(expectedMap, map);
    }

    @Test
    void loadMapFromResource() throws IOException
    {
        Map<String, String> map = ContainerLoader.load("com/noleme/commons/file/map.txt", new HashMap<>(), new MapHandler("="));

        Assertions.assertEquals(expectedMap, map);
    }

    @Test
    void loadMapFromStream() throws IOException
    {
        Map<String, String> map = ContainerLoader.load("com/noleme/commons/file/map.txt", Resources::streamFrom, new HashMap<>(), new MapHandler("="));

        Assertions.assertEquals(expectedMap, map);
    }
}

