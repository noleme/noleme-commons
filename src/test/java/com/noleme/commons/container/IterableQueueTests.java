package com.noleme.commons.container;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/12/2019
 */
public class IterableQueueTests
{
    private static final List<Integer> expectedList = Lists.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

    @Test
    void loadCollectionFromFile()
    {
        List<Integer> l1 = Lists.of(0, 1, 2, 3);
        List<Integer> l2 = Lists.of(4, 5, 6);
        List<Integer> l3 = Lists.of(7, 8, 9);

        IteratorQueue<Integer> q = new IteratorQueue<>(l1, l2, l3);
        List<Integer> compiled = new ArrayList<>();

        while (q.hasNext())
            compiled.add(q.next());

        Assertions.assertEquals(expectedList, compiled);
    }
}
