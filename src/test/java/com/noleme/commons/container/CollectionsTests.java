package com.noleme.commons.container;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public class CollectionsTests
{
    private static final List<String> baseList = Lists.of("abc", "def", "ghi");

    @Test
    void addAllIterator()
    {
        List<String> copy = new ArrayList<>();

        boolean modified = Collections.addAll(copy, baseList.iterator());

        Assertions.assertEquals(true, modified);
        Assertions.assertEquals(baseList, copy);
    }

    @Test
    void addAllIteratorSkipOne()
    {
        List<String> copy = new ArrayList<>();

        Iterator<String> it = baseList.iterator();
        it.next();

        boolean modified = Collections.addAll(copy, it);

        Assertions.assertEquals(true, modified);
        Assertions.assertEquals(Lists.of("def", "ghi"), copy);
    }

    @Test
    void addAllCollection()
    {
        List<String> copy = new ArrayList<>();

        boolean modified = Collections.addAll(copy, baseList);

        Assertions.assertEquals(true, modified);
        Assertions.assertEquals(baseList, copy);
    }

    @Test
    void addAllCollectionEmpty()
    {
        List<String> copy = new ArrayList<>();

        boolean modified = Collections.addAll(copy, new ArrayList<>());

        Assertions.assertEquals(false, modified);
        Assertions.assertEquals(new ArrayList<>(), copy);
    }
}
