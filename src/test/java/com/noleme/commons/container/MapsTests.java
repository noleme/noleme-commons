package com.noleme.commons.container;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 09/12/2019
 */
public class MapsTests
{
    private static final Map<String, Integer> baseMap = Maps.of(
        "meuh", 2,
        "meh", 5,
        "moo", 4,
        "mooo", 5,
        "mooh", 3
    );

    @Test
    void reverse()
    {
        Map<Integer, List<String>> reversed = Maps.of(
            2, Lists.of("meuh"),
            4, Lists.of("moo"),
            5, Lists.of("meh", "mooo"),
            3, Lists.of("mooh")
        );

        Assertions.assertEquals(reversed, Maps.reverse(baseMap));
    }

    @Test
    void reverseUnique()
    {
        Map<Integer, String> reversed = Maps.of(
            2, "meuh",
            4, "moo",
            5, "mooo",
            3, "mooh"
        );

        Assertions.assertEquals(reversed, Maps.reverseUnique(baseMap));
    }

    @Test
    void unmap()
    {
        List<String> keys = new ArrayList<>(baseMap.keySet());
        List<Integer> values = new ArrayList<>(baseMap.values());

        Pair<List<String>, List<Integer>> unmapped = Maps.unmap(baseMap);

        Assertions.assertEquals(keys, unmapped.first);
        Assertions.assertEquals(values, unmapped.second);
    }

    @Test
    void unmapUnsafe()
    {
        Map<String, Object> map = Maps.of(
            "meuh", 2,
            "meh", Lists.of("mooh", "mah"),
            "moo", 5,
            "mooo", "well",
            "mooh", 7
        );

        List<String> keys = new ArrayList<>(map.keySet());
        List<Object> values = new ArrayList<>(map.values());

        /* This method is a bit of a weird one, it's about type safety more than anything else */
        Pair<List<String>, List<Integer>> unmapped = Maps.unmapUnsafe(map);

        Assertions.assertEquals(keys, unmapped.first);
        Assertions.assertEquals(values, unmapped.second);
    }
}
