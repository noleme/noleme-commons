package com.noleme.commons.container;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public class EvictingQueueTests
{
    @Test
    void evictingQueueAdd()
    {
        EvictingQueue<String> queue = new EvictingQueue<>(3);
        queue.add("abc");
        queue.add("def");

        List<String> compiled = new ArrayList<>();

        Collections.addAll(compiled, queue);

        Assertions.assertEquals(Lists.of("abc", "def"), compiled);
        Assertions.assertEquals(1, queue.remainingCapacity());
    }

    @Test
    void evictingQueueAddOverflow()
    {
        EvictingQueue<String> queue = new EvictingQueue<>(3);
        queue.add("abc");
        queue.add("def");
        queue.add("ghi");
        queue.add("jkl");

        List<String> compiled = new ArrayList<>();

        Collections.addAll(compiled, queue);

        Assertions.assertEquals(Lists.of("def", "ghi", "jkl"), compiled);
        Assertions.assertEquals(0, queue.remainingCapacity());
    }
}
