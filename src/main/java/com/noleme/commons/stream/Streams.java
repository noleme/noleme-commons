package com.noleme.commons.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 09/05/2017
 */
final public class Streams
{
    private Streams()
    {
        throw new Error("The Streams class is not meant to be instantiated.");
    }

    /**
     *
     * @param is
     * @param os
     * @throws IOException
     */
    public static void flow(InputStream is, OutputStream os) throws IOException
    {
        flow(is, os, 1024);
    }

    /**
     *
     * @param is
     * @param os
     * @param policy
     * @throws IOException
     */
    public static void flow(InputStream is, OutputStream os, Policy policy) throws IOException
    {
        flow(is, os, 1024, policy);
    }

    /**
     *
     * @param is
     * @param os
     * @param bufferSize
     * @throws IOException
     */
    public static void flow(InputStream is, OutputStream os, int bufferSize) throws IOException
    {
        flow(is, os, bufferSize, Policy.CLOSE_BOTH);
    }

    /**
     *
     * @param is
     * @param os
     * @param bufferSize
     * @param policy
     * @throws IOException
     */
    public static void flow(InputStream is, OutputStream os, int bufferSize, Policy policy) throws IOException
    {
        try {
            byte[] buffer = new byte[bufferSize];
            int read;

            while ((read = is.read(buffer)) > 0)
                os.write(buffer, 0, read);
        }
        finally {
            if (policy.closeInput && is != null)
                is.close();
            if (policy.closeOutput && os != null)
                os.close();
        }
    }

    public enum Policy
    {
        CLOSE_BOTH(true, true),
        CLOSE_NONE(false, false),
        CLOSE_INPUT(true, false),
        CLOSE_OUTPUT(false, true),
        ;

        private boolean closeInput;
        private boolean closeOutput;

        Policy(boolean closeIn, boolean closeOut)
        {
            this.closeInput = closeIn;
            this.closeOutput = closeOut;
        }
    }
}

