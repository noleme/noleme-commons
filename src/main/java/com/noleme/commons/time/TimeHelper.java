package com.noleme.commons.time;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 03/02/2017
 */
public final class TimeHelper
{
    private TimeHelper()
    {
    }

    /**
     *
     * @return
     */
    public static Instant midnightToday()
    {
        return Instant.now().truncatedTo(ChronoUnit.DAYS);
    }

    /**
     *
     * @param minutes
     * @return
     */
    public static Instant inSeconds(long minutes)
    {
        return in(minutes, ChronoUnit.SECONDS);
    }

    /**
     *
     * @param minutes
     * @return
     */
    public static Instant inMinutes(long minutes)
    {
        return in(minutes, ChronoUnit.MINUTES);
    }

    /**
     *
     * @param hours
     * @return
     */
    public static Instant inHours(long hours)
    {
        return in(hours, ChronoUnit.HOURS);
    }

    /**
     *
     * @param days
     * @return
     */
    public static Instant inDays(long days)
    {
        return in(days, ChronoUnit.DAYS);
    }

    /**
     *
     * @param years
     * @return
     */
    public static Instant inYears(long years)
    {
        return in(years, ChronoUnit.YEARS);
    }

    /**
     *
     * @param offset
     * @param unit
     * @return
     */
    public static Instant in(long offset, ChronoUnit unit)
    {
        Instant instant = Instant.now();
        if (offset != 0)
            return instant.plus(offset, unit);
        return instant;
    }
}
