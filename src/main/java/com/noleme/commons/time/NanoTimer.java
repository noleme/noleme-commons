package com.noleme.commons.time;

/**
 * @author Pierre Lecerf (pierre@noleme.com) on 28/08/15.
 */
public class NanoTimer extends Timer
{
    public NanoTimer()
    {
        super();
    }

    /**
     *
     * @param label
     */
    public NanoTimer(String label)
    {
        super(label);
    }

    /**
     *
     * @param label
     * @param verbose
     */
    public NanoTimer(String label, boolean verbose)
    {
        super(label, verbose);
    }

    /**
     *
     * @return
     */
    protected long unixTime()
    {
        return System.nanoTime();
    }

    /**
     *
     * @return
     */
    protected final String unit()
    {
        return "ns";
    }
}
