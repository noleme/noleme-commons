package com.noleme.commons.time;

/**
 * @author Pierre Lecerf (pierre@noleme.com) on 29/08/2015.
 */
public class MilliTimer extends Timer
{
    public MilliTimer()
    {
        super();
    }

    /**
     *
     * @param label
     */
    public MilliTimer(String label)
    {
        super(label);
    }

    /**
     *
     * @param label
     * @param verbose
     */
    public MilliTimer(String label, boolean verbose)
    {
        super(label, verbose);
    }

    /**
     *
     * @return
     */
    protected long unixTime()
    {
        return System.nanoTime() / 1000000;
    }

    /**
     *
     * @return
     */
    protected final String unit()
    {
        return "ms";
    }
}
