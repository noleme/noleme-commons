package com.noleme.commons.time;

import com.noleme.commons.time.print.DefaultPrinter;
import com.noleme.commons.time.print.TimePrinter;

import java.util.Date;

/**
 * @author Pierre Lecerf (pierre@noleme.com) on 07/03/15.
 */
public abstract class Timer
{
    private String label;
    private boolean verbose;
    private TimePrinter printer;
    protected long start;
    protected long total;
    protected long mark;

    public static TimePrinter defaultPrinter = new DefaultPrinter();

    public Timer()
    {
        this(null);
    }

    /**
     *
     * @param label
     */
    public Timer(String label)
    {
        this(label, false);
    }

    /**
     *
     * @param label
     * @param verbose
     */
    public Timer(String label, boolean verbose)
    {
        this(label, verbose, defaultPrinter);
    }

    /**
     *
     * @param label
     * @param verbose
     * @param printer
     */
    public Timer(String label, boolean verbose, TimePrinter printer)
    {
        this.label = label;
        this.verbose = verbose;
        this.start = 0;
        this.total = 0;
        this.mark = 0;
        this.printer = printer;
    }

    /**
     *
     * @return Elapsed time since start
     */
    public long resume()
    {
        return this.resume(this.verbose);
    }

    /**
     * @param verbose
     * @return Elapsed time since start
     */
    public long resume(boolean verbose)
    {
        this.mark = unixTime();
        if (verbose)
            this.printer.print("Resumed "+(this.label != null ? this.label+" timer " : "")+"at "+(new Date())+", elapsed time is "+this.total+this.unit());
        return this.total;
    }

    /**
     *
     * @return Elapsed time since last start/resume
     */
    public long pause()
    {
        return this.pause(this.verbose);
    }

    /**
     * @param verbose
     * @return Elapsed time since last start/resume
     */
    public long pause(boolean verbose)
    {
        long now = unixTime();
        long elapsed = 0;
        if (this.mark > 0)
        {
            elapsed = now - this.mark;
            this.total += elapsed;
        }
        this.mark = 0;
        if (verbose)
            this.printer.print("Paused "+(this.label != null ? this.label+" timer " : "")+"at "+(new Date())+", elapsed time is "+this.total+this.unit()+" (+"+elapsed+this.unit()+" since last measured)");
        return elapsed;
    }

    /**
     *
     */
    public Timer start()
    {
        return this.start(this.verbose);
    }

    /**
     *
     * @param verbose
     */
    public Timer start(boolean verbose)
    {
        long now = unixTime();
        this.start = now;
        this.mark = now;
        this.total = 0;
        if (verbose)
            this.printer.print("Started "+(this.label != null ? this.label+" timer " : "")+"at "+(new Date()));
        return this;
    }

    /**
     * @return Elapsed time since start
     */
    public long stop()
    {
        return this.stop(this.verbose);
    }

    /**
     *
     * @param verbose
     * @return Elapsed time since start
     */
    public long stop(boolean verbose)
    {
        long now = unixTime();
        long elapsed = 0;
        if (this.mark > 0)
        {
            elapsed = now - this.mark;
            this.total += elapsed;
        }
        long total = this.total;
        if (verbose)
            this.printer.print("Stopped "+(this.label != null ? this.label+" timer " : "")+"at "+(new Date())+", elapsed time is "+total+this.unit()+" (+"+elapsed+this.unit()+" since last measured)");
        this.total = 0;
        this.mark = 0;
        return total;
    }

    /**
     *
     * @return Elapsed time since last start/resume
     */
    public long getElapsed()
    {
        if (this.mark == 0)
            return 0;
        return unixTime() - this.mark;
    }

    /**
     *
     * @return Elapsed time since start
     */
    public long getTime()
    {
        return this.total + this.getElapsed();
    }
    public long getStart()
    {
        return this.start;
    }

    /**
     *
     * @param v
     * @return
     */
    public Timer setVerbose(boolean v)
    {
        this.verbose = v;
        return this;
    }

    protected abstract long unixTime();

    protected abstract String unit();
}
