package com.noleme.commons.time.print;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 06/11/2020
 */
public interface TimePrinter
{
    /**
     *
     * @param message
     */
    void print(String message);
}
