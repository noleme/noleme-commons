package com.noleme.commons.time.print;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 06/11/2020
 */
public class DefaultPrinter implements TimePrinter
{
    @Override
    public void print(String message)
    {
        System.out.println(message);
    }
}
