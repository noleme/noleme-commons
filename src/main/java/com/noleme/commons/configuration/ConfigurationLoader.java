package com.noleme.commons.configuration;

import java.util.Map;
import java.util.Properties;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/01/2018
 */
public class ConfigurationLoader
{
    /**
     *
     * @param props
     * @param config
     * @return
     */
    public static <C extends Configuration<C>> C load(Properties props, C config)
    {
        for (Map.Entry<Object, Object> e : props.entrySet())
        {
            String key = (String)e.getKey();
            String value = (String)e.getValue();
            if (isInteger(value))
                config.set(key, Integer.parseInt(value));
            else if (isNumeric(value))
                config.set(key, Double.parseDouble(value));
            else if (isBoolean(value))
                config.set(key, Boolean.parseBoolean(value));
            else
                config.set(key, extractString(value));
        }

        return config;
    }

    protected static boolean isInteger(String str)
    {
        return str.matches("^-?\\d+$");
    }

    protected static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    protected static boolean isBoolean(String str)
    {
        return str.equals("true") || str.equals("false");
    }

    protected static String extractString(String str)
    {
        if (str.length() >= 2 && str.startsWith("\"") && str.endsWith("\""))
            return str.substring(1, str.length() - 1);
        return str;
    }
}
