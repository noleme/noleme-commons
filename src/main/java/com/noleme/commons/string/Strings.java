package com.noleme.commons.string;

/**
 * A bit of a weird one here, this currently hosts a port of org.apache.commons.lang3.StringUtils padding functions.
 * We use padding functions relatively commonly, while we typically don't need the whole rest of the apache commons package.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 2020/07/07
 */
public class Strings
{
    /**
     *
     * @param str
     * @param size
     * @return
     */
    public static String padRight(String str, int size)
    {
        return padRight(str, size, ' ');
    }

    /**
     *
     * @param str
     * @param size
     * @param padChar
     * @return
     */
    public static String padRight(String str, int size, char padChar)
    {
        if (str == null)
            return null;

        int pads = size - str.length();

        if (pads <= 0)
            return str;

        return pads > 8192 ? padRight(str, size, String.valueOf(padChar)) : str.concat(repeat(padChar, pads));
    }

    /**
     *
     * @param str
     * @param size
     * @param padStr
     * @return
     */
    public static String padRight(String str, int size, String padStr)
    {
        if (str == null)
            return null;

        if (padStr.isEmpty())
            padStr = " ";

        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;

        if (pads <= 0)
            return str;
        else if (padLen == 1 && pads <= 8192)
            return padRight(str, size, padStr.charAt(0));
        else if (pads == padLen)
            return str.concat(padStr);
        else if (pads < padLen)
            return str.concat(padStr.substring(0, pads));

        char[] padding = new char[pads];
        char[] padChars = padStr.toCharArray();

        for (int i = 0; i < pads; ++i)
            padding[i] = padChars[i % padLen];

        return str.concat(new String(padding));
    }

    /**
     *
     * @param str
     * @param size
     * @return
     */
    public static String padLeft(String str, int size)
    {
        return padLeft(str, size, ' ');
    }

    /**
     *
     * @param str
     * @param size
     * @param padChar
     * @return
     */
    public static String padLeft(String str, int size, char padChar)
    {
        if (str == null)
            return null;

        int pads = size - str.length();
        if (pads <= 0)
            return str;
        return pads > 8192 ? padLeft(str, size, String.valueOf(padChar)) : repeat(padChar, pads).concat(str);
    }

    /**
     *
     * @param str
     * @param size
     * @param padStr
     * @return
     */
    public static String padLeft(String str, int size, String padStr)
    {
        if (str == null)
            return null;

        if (padStr.isEmpty())
            padStr = " ";

        int padLen = padStr.length();
        int strLen = str.length();
        int pads = size - strLen;

        if (pads <= 0)
            return str;
        else if (padLen == 1 && pads <= 8192)
            return padLeft(str, size, padStr.charAt(0));
        else if (pads == padLen)
            return padStr.concat(str);
        else if (pads < padLen)
            return padStr.substring(0, pads).concat(str);

        char[] padding = new char[pads];
        char[] padChars = padStr.toCharArray();

        for (int i = 0; i < pads; ++i)
            padding[i] = padChars[i % padLen];

        return (new String(padding)).concat(str);
    }

    /**
     *
     * @param str
     * @param repeat
     * @return
     */
    public static String repeat(String str, int repeat)
    {
        if (str == null)
            return null;
        else if (repeat <= 0)
            return "";

        int inputLength = str.length();
        if (repeat != 1 && inputLength != 0)
        {
            if (inputLength == 1 && repeat <= 8192)
                return repeat(str.charAt(0), repeat);

            int outputLength = inputLength * repeat;
            switch (inputLength)
            {
                case 1:
                    return repeat(str.charAt(0), repeat);
                case 2:
                    char ch0 = str.charAt(0);
                    char ch1 = str.charAt(1);
                    char[] output2 = new char[outputLength];

                    for (int i = repeat * 2 - 2; i >= 0; --i)
                    {
                        output2[i] = ch0;
                        output2[i + 1] = ch1;
                        --i;
                    }

                    return new String(output2);
                default:
                    StringBuilder buf = new StringBuilder(outputLength);

                    for (int i = 0; i < repeat; ++i)
                        buf.append(str);

                    return buf.toString();
            }
        }

        return str;
    }

    /**
     *
     * @param ch
     * @param repeat
     * @return
     */
    public static String repeat(char ch, int repeat)
    {
        if (repeat <= 0)
            return "";

        char[] buf = new char[repeat];

        for (int i = repeat - 1; i >= 0; --i)
            buf[i] = ch;

        return new String(buf);
    }
}
