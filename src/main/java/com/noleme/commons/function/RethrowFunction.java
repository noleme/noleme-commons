package com.noleme.commons.function;

import java.util.function.Function;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 23/11/2019
 */
@FunctionalInterface
public interface RethrowFunction<T, R, E extends Exception>
{
    /**
     *
     * @param input
     * @return
     * @throws E
     */
    R apply(T input) throws E;

    /**
     *
     * @param function
     * @param <T>
     * @param <R>
     * @param <E>
     * @return
     * @throws E
     */
    static <T, R, E extends Exception> Function<T, R> rethrower(RethrowFunction<T, R, E> function) throws E
    {
        return t -> {
            try {
                return function.apply(t);
            }
            catch (Exception e) {
                Rethrows.throwAsUnchecked(e);
                return null;
            }
        };
    }
}
