package com.noleme.commons.function;

import java.util.function.Predicate;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 27/04/2020
 */
@FunctionalInterface
public interface RethrowPredicate<T, E extends Exception>
{
    /**
     *
     * @param input
     * @return
     * @throws E
     */
    boolean apply(T input) throws E;

    /**
     *
     * @param predicate
     * @param <T>
     * @param <E>
     * @return
     * @throws E
     */
    static <T, E extends Exception> Predicate<T> rethrower(RethrowPredicate<T, E> predicate) throws E
    {
        return t -> {
            try {
                return predicate.apply(t);
            }
            catch (Exception e) {
                Rethrows.throwAsUnchecked(e);
                return false;
            }
        };
    }
}
