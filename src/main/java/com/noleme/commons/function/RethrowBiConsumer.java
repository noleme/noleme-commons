package com.noleme.commons.function;

import java.util.function.BiConsumer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 23/11/2019
 */
@FunctionalInterface
public interface RethrowBiConsumer<T, U, E extends Exception>
{
    /**
     *
     * @param input1
     * @param input2
     * @throws E
     */
    void accept(T input1, U input2) throws E;

    /**
     *
     * @param consumer
     * @param <T>
     * @param <U>
     * @param <E>
     * @return
     * @throws E
     */
    static <T, U, E extends Exception> BiConsumer<T, U> rethrower(RethrowBiConsumer<T, U, E> consumer) throws E
    {
        return (input1, input2) -> {
            try {
                consumer.accept(input1, input2);
            }
            catch (Exception e) {
                Rethrows.throwAsUnchecked(e);
            }
        };
    }
}
