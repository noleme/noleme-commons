package com.noleme.commons.function;

import java.util.function.Consumer;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 23/11/2019
 */
@FunctionalInterface
public interface RethrowConsumer<T, E extends Exception>
{
    /**
     *
     * @param input
     * @throws E
     */
    void accept(T input) throws E;

    /**
     *
     * @param consumer
     * @param <T>
     * @param <E>
     * @return
     * @throws E
     */
    static <T, E extends Exception> Consumer<T> rethrower(RethrowConsumer<T, E> consumer) throws E
    {
        return input -> {
            try {
                consumer.accept(input);
            }
            catch (Exception e) {
                Rethrows.throwAsUnchecked(e);
            }
        };
    }
}
