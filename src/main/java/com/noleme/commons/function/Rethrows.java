package com.noleme.commons.function;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 23/11/2019
 */
public final class Rethrows
{
    private Rethrows() {}

    /**
     *
     * @param exception
     * @param <E>
     * @throws E
     */
    @SuppressWarnings("unchecked")
    public static <E extends Throwable> void throwAsUnchecked(Exception exception) throws E
    {
        throw (E)exception;
    }
}
