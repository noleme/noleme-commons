package com.noleme.commons.function;

import java.util.function.Supplier;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 23/11/2019
 */
@FunctionalInterface
public interface RethrowSupplier<T, E extends Exception>
{
    /**
     *
     * @return
     * @throws E
     */
    T get() throws E;

    /**
     *
     * @param supplier
     * @param <T>
     * @param <E>
     * @return
     * @throws E
     */
    static <T, E extends Exception> Supplier<T> rethrower(RethrowSupplier<T, E> supplier) throws E
    {
        return () -> {
            try {
                return supplier.get();
            }
            catch (Exception e) {
                Rethrows.throwAsUnchecked(e);
                return null;
            }
        };
    }
}
