package com.noleme.commons.file;

import com.noleme.commons.stream.Streams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 20/10/2018
 */
final public class Resources
{
    private Resources()
    {
        throw new Error("The Resources class is not meant to be instantiated.");
    }

    /**
     *
     * @param path
     * @return
     */
    public static boolean exists(String path)
    {
        return exists(path, Resources.class.getClassLoader());
    }

    /**
     *
     * @param path
     * @param classLoader
     * @return
     */
    public static boolean exists(String path, ClassLoader classLoader)
    {
        return classLoader.getResourceAsStream(path) != null;
    }

    /**
     *
     * @param path
     * @return
     */
    public static String readFrom(String path) throws IOException
    {
        return readFrom(path, Charsets.UTF_8.getCharset());
    }

    /**
     *
     * @param path
     * @param charset
     * @return
     */
    public static String readFrom(String path, Charset charset) throws IOException
    {
        return readFrom(path, charset, Resources.class.getClassLoader());
    }

    /**
     *
     * @param path
     * @param charset
     * @param classLoader
     * @return
     * @throws IOException
     */
    public static String readFrom(String path, Charset charset, ClassLoader classLoader) throws IOException
    {
        InputStream stream = streamFrom(path, classLoader);

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        Streams.flow(stream, os);

        return new String(os.toByteArray(), charset);
    }

    /**
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static InputStream streamFrom(String path) throws IOException
    {
        return streamFrom(path, Resources.class.getClassLoader());
    }

    /**
     *
     * @param path
     * @param classLoader
     * @return
     * @throws IOException
     */
    public static InputStream streamFrom(String path, ClassLoader classLoader) throws IOException
    {
        InputStream stream = classLoader.getResourceAsStream(path);

        if (stream == null)
            throw new IOException("The specified resource could not be found at " + path);

        return stream;
    }
}
