package com.noleme.commons.file;

import com.noleme.commons.stream.Streams;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 09/05/2017
 */
final public class Files
{
    private Files()
    {
        throw new Error("The Files class is not meant to be instantiated.");
    }

    /**
     *
     * @param path
     * @return
     */
    public static boolean fileExists(String path)
    {
        return new File(path).isFile();
    }

    /**
     *
     * @param path
     * @return
     */
    public static boolean directoryExists(String path)
    {
        return new File(path).isDirectory();
    }

    /**
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String readFrom(String path) throws IOException
    {
        return readFrom(path, Charsets.UTF_8.getCharset());
    }

    /**
     *
     * @param path
     * @param charset
     * @return
     * @throws IOException
     */
    public static String readFrom(String path, Charset charset) throws IOException
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        Streams.flow(new FileInputStream(path), os);

        return new String(os.toByteArray(), charset);
    }

    /**
     *
     * @param path
     * @return
     */
    public static InputStream streamFrom(String path) throws FileNotFoundException
    {
        return new FileInputStream(path);
    }

    /**
     *
     * @param path
     * @param contents
     * @throws IOException
     */
    public static void writeTo(String path, String contents) throws IOException
    {
        writeTo(path, contents, false, Charsets.UTF_8.getCharset());
    }

    /**
     *
     * @param path
     * @param contents
     * @param append
     * @throws IOException
     */
    public static void writeTo(String path, String contents, boolean append) throws IOException
    {
        writeTo(path, contents, append, Charsets.UTF_8.getCharset());
    }

    /**
     *
     * @param path
     * @param contents
     * @param append
     * @param charset
     * @throws IOException
     */
    public static void writeTo(String path, String contents, boolean append, Charset charset) throws IOException
    {
        Path filePath = Paths.get(path);
        Path directoryPath = filePath.getParent();

        if (directoryPath != null && java.nio.file.Files.notExists(directoryPath))
            java.nio.file.Files.createDirectories(directoryPath);

        Streams.flow(new ByteArrayInputStream(contents.getBytes(charset)), new FileOutputStream(path, append));
    }
}
