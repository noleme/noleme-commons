package com.noleme.commons.file.loader;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 07/12/2017
 */
public class MapHandler extends com.noleme.commons.file.loader.flexible.MapHandler<String, String>
{
    /**
     * Builds a MapHandler with keyMapper and valueMapper functions that both return the string as present in the source file.
     *
     * @param separator
     */
    public MapHandler(String separator)
    {
        super(separator, s -> s, s -> s);
    }
}

