package com.noleme.commons.file.loader;

import com.noleme.commons.file.loader.file.DynamicLoader;
import com.noleme.commons.file.loader.file.FileLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 07/12/2017
 */
public final class ContainerLoader
{
    private final static FileLoader defaultLoader = new DynamicLoader();

    private ContainerLoader()
    {
        throw new Error("The ContainerLoader class is not meant to be instantiated.");
    }

    /**
     *
     * @param path
     * @param container
     * @param handler
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T load(String path, T container, LineHandler<T> handler) throws IOException
    {
        return load(path, defaultLoader, container, handler);
    }

    /**
     *
     * @param path
     * @param fileLoader
     * @param container
     * @param handler
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T load(String path, FileLoader fileLoader, T container, LineHandler<T> handler) throws IOException
    {
        return load(fileLoader.load(path), container, handler);
    }

    /**
     *
     * @param stream
     * @param container
     * @param handler
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T> T load(InputStream stream, T container, LineHandler<T> handler) throws IOException
    {
        if (stream == null)
            return container;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(stream)))
        {
            for (String l = br.readLine(); l != null; l = br.readLine())
            {
                if (l.trim().startsWith("#"))
                    continue;

                handler.process(l, container);
            }
        }

        return container;
    }
}

