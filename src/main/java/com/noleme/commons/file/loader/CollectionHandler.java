package com.noleme.commons.file.loader;

import java.util.Collection;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 07/12/2017
 */
public class CollectionHandler<C extends Collection<String>> extends com.noleme.commons.file.loader.flexible.CollectionHandler<String, C>
{
    /**
     * Builds a CollectionHandler with valueMapper function that returns the string as present in the source file.
     */
    public CollectionHandler()
    {
        super(s -> s);
    }
}
