package com.noleme.commons.file.loader.file;

import com.noleme.commons.file.Files;
import com.noleme.commons.file.Resources;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author eledhwen
 * Created on 11/12/2018
 */
public class DynamicLoader implements FileLoader
{
    @Override
    public InputStream load(String path) throws IOException
    {
        return Resources.exists(path) ? Resources.streamFrom(path) : Files.streamFrom(path);
    }
}
