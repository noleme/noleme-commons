package com.noleme.commons.file.loader;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 07/12/2017
 */
public interface LineHandler <T>
{
    /**
     *
     * @param line
     * @param container
     */
    void process(String line, T container);
}
