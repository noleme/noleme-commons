package com.noleme.commons.file.loader.flexible;

import com.noleme.commons.file.loader.LineHandler;

import java.util.Map;
import java.util.function.Function;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 2020/04/02
 */
public class MapHandler<K, V> implements LineHandler<Map<K, V>>
{
    private final String separator;
    private final Function<String, K> keyMapper;
    private final Function<String, V> valueMapper;

    /**
     *
     * @param separator
     * @param keyMapper
     * @param valueMapper
     */
    public MapHandler(String separator, Function<String, K> keyMapper, Function<String, V> valueMapper)
    {
        this.separator = separator;
        this.keyMapper = keyMapper;
        this.valueMapper = valueMapper;
    }

    /**
     * @param line
     * @param container
     */
    @Override
    public void process(String line, Map<K, V> container)
    {
        String[] e = line.split(this.separator, 2);

        if (e.length == 2)
            container.put(this.keyMapper.apply(e[0]), this.valueMapper.apply(e[1]));
        else
            container.put(this.keyMapper.apply(e[0]), null);
    }
}

