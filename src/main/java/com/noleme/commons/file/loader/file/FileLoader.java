package com.noleme.commons.file.loader.file;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 11/12/2018
 */
public interface FileLoader
{
    /**
     *
     * @param path
     * @return
     * @throws IOException
     */
    InputStream load(String path) throws IOException;
}
