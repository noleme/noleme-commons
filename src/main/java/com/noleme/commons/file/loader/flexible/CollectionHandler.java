package com.noleme.commons.file.loader.flexible;

import com.noleme.commons.file.loader.LineHandler;

import java.util.Collection;
import java.util.function.Function;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 2020/04/02
 */
public class CollectionHandler<T, C extends Collection<T>> implements LineHandler<C>
{
    private final Function<String, T> valueMapper;

    /**
     *
     * @param valueMapper
     */
    public CollectionHandler(Function<String, T> valueMapper)
    {
        this.valueMapper = valueMapper;
    }

    /**
     * @param line
     * @param container
     */
    @Override
    public void process(String line, C container)
    {
        container.add(this.valueMapper.apply(line));
    }
}
