package com.noleme.commons.file;

import java.nio.charset.Charset;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 2020/02/26
 */
public enum Charsets
{
    ASCII("ASCII"),
    US_ASCII("US-ASCII"),
    UTF_8("UTF-8"),
    UTF_16("UTF-16"),
    UTF_32("UTF-32"),
    ISO_LATIN_1("ISO-8859-1"),
    ISO_LATIN_2("ISO-8859-2"),
    ISO_LATIN_4("ISO-8859-4"),
    ISO_CYRILLIC("ISO-8859-5"),
    ISO_GREEK("ISO-8859-7"),
    ISO_LATIN_5("ISO-8859-9"),
    ISO_LATIN_7("ISO-8859-13"),
    ISO_LATIN_9("ISO-8859-15"),
    WIN_EASTERN_EUROPEAN("windows-1250"),
    WIN_CYRILLIC("windows-1251"),
    WIN_LATIN_1("windows-1252"),
    WIN_GREEK("windows-1253"),
    WIN_TURKISH("windows-1254"),
    WIN_BALTIC("windows-1255"),
    MSDOS_LATIN_1("IBM850"),
    MSDOS_LATIN_2("IBM852"),
    MSDOS_RUSSIAN("IBM866"),
    ;

    private final Charset charset;
    private final String name;

    Charsets(String name)
    {
        this.name = name;
        this.charset = Charset.forName(name);
    }

    public Charset getCharset()
    {
        return charset;
    }

    public String getName()
    {
        return this.name;
    }
}
