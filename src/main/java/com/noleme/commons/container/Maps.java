package com.noleme.commons.container;

import java.util.*;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 19/01/2017
 */
final public class Maps
{
    private Maps()
    {
        throw new Error("The Maps class is not meant to be instanced.");
    }

    /**
     *
     * @param keys
     * @param values
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> map(Collection<K> keys, Collection<V> values)
    {
        Map<K, V> map = new HashMap<>();
        Iterator<K> keyIterator = keys.iterator();
        Iterator<V> valueIterator = values.iterator();
        while (keyIterator.hasNext() && valueIterator.hasNext())
            map.put(keyIterator.next(), valueIterator.next());
        return map;
    }

    /**
     *
     * @param keys
     * @param values
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> mapMatch(Collection<K> keys, Collection<V> values, V filler)
    {
        Map<K, V> map = new HashMap<>();
        Iterator<K> keyIterator = keys.iterator();
        Iterator<V> valueIterator = values.iterator();
        while (keyIterator.hasNext())
        {
            V next = valueIterator.hasNext() ? valueIterator.next() : filler;
            map.put(keyIterator.next(), next);
        }
        return map;
    }

    /**
     *
     * @param map
     * @param <K>
     * @param <V>
     */
    public static <K, V> Pair<List<K>, List<V>> unmap(Map<? extends K, ? extends V> map)
    {
        List<K> keys = new ArrayList<>();
        List<V> values = new ArrayList<>();
        unmap(map, keys, values);
        return new Pair<>(keys, values);
    }

    /**
     *
     * @param map
     * @param keys
     * @param values
     * @param <K>
     * @param <V>
     */
    public static <K, V> void unmap(Map<? extends K, ? extends V> map, Collection<K> keys, Collection<V> values)
    {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet())
        {
            keys.add(entry.getKey());
            values.add(entry.getValue());
        }
    }

    /**
     *
     * @param map
     * @param <K>
     * @param <V>
     */
    public static <K, V> Pair<List<K>, List<V>> unmapUnsafe(Map<?, ?> map)
    {
        List<K> keys = new ArrayList<>();
        List<V> values = new ArrayList<>();
        unmapUnsafe(map, keys, values);
        return new Pair<>(keys, values);
    }

    /**
     *
     * @param map
     * @param keys
     * @param values
     * @param <K>
     * @param <V>
     */
    public static <K, V> void unmapUnsafe(Map<?, ?> map, Collection<K> keys, Collection<V> values)
    {
        for (Map.Entry<?, ?> entry : map.entrySet())
        {
            keys.add((K)entry.getKey());
            values.add((V)entry.getValue());
        }
    }

    /**
     *
     * @param map
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<V, List<K>> reverse(Map<K, V> map)
    {
        Map<V, List<K>> reversed = new HashMap<>();

        for (Map.Entry<K, V> entry : map.entrySet())
        {
            if (!reversed.containsKey(entry.getValue()))
                reversed.put(entry.getValue(), new ArrayList<K>());
            reversed.get(entry.getValue()).add(entry.getKey());
        }

        return reversed;
    }

    /**
     *
     * @param map
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<V, K> reverseUnique(Map<K, V> map)
    {
        Map<V, K> reversed = new HashMap<>();

        for (Map.Entry<K, V> entry : map.entrySet())
            reversed.put(entry.getValue(), entry.getKey());

        return reversed;
    }

    /**
     *
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of()
    {
        return new HashMap<>();
    }

    /**
     *
     * @param k1
     * @param v1
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param k8
     * @param v8
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8)
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        map.put(k8, v8);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param k8
     * @param v8
     * @param k9
     * @param v9
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(
            K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9
    )
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        map.put(k8, v8);
        map.put(k9, v9);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param k8
     * @param v8
     * @param k9
     * @param v9
     * @param k10
     * @param v10
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(
            K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9, K k10, V v10
    )
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        map.put(k8, v8);
        map.put(k9, v9);
        map.put(k10, v10);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param k8
     * @param v8
     * @param k9
     * @param v9
     * @param k10
     * @param v10
     * @param k11
     * @param v11
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(
            K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9, K k10, V v10, K k11,
            V v11
    )
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        map.put(k8, v8);
        map.put(k9, v9);
        map.put(k10, v10);
        map.put(k11, v11);
        return map;
    }

    /**
     *
     * @param k1
     * @param v1
     * @param k2
     * @param v2
     * @param k3
     * @param v3
     * @param k4
     * @param v4
     * @param k5
     * @param v5
     * @param k6
     * @param v6
     * @param k7
     * @param v7
     * @param k8
     * @param v8
     * @param k9
     * @param v9
     * @param k10
     * @param v10
     * @param k11
     * @param v11
     * @param k12
     * @param v12
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> of(
            K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7, K k8, V v8, K k9, V v9, K k10, V v10, K k11,
            V v11, K k12, V v12
    )
    {
        Map<K, V> map = new HashMap<>();
        map.put(k1, v1);
        map.put(k2, v2);
        map.put(k3, v3);
        map.put(k4, v4);
        map.put(k5, v5);
        map.put(k6, v6);
        map.put(k7, v7);
        map.put(k8, v8);
        map.put(k9, v9);
        map.put(k10, v10);
        map.put(k11, v11);
        map.put(k12, v12);
        return map;
    }

}
