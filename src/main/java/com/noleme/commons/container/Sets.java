package com.noleme.commons.container;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/08/2019
 */
final public class Sets
{
    private Sets()
    {
        throw new Error("The Sets class is not meant to be instanced.");
    }

    /**
     *
     * @param values
     * @param <T>
     * @return A modifiable Set collection containing the provided values.
     */
    public static <T> Set<T> of(T... values)
    {
        return new HashSet<>(Arrays.asList(values));
    }

    /**
     *
     * @param container
     * @param values
     * @param <S>
     * @param <T>
     * @return The provided Set collection with the provided values added.
     */
    public static <S extends Set<T>, T> S of(S container, T... values)
    {
        container.addAll(Arrays.asList(values));
        return container;
    }
}
