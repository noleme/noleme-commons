package com.noleme.commons.container;

import java.util.Iterator;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public final class Iterators
{
    private Iterators() {}

    /**
     *
     * @param iterable
     * @param skip
     * @param <T>
     * @return
     */
    public static <T> Iterator<T> skip(Iterable<T> iterable, int skip)
    {
        Iterator<T> it = iterable.iterator();

        for (int i = 0 ; i < skip ; ++i)
            it.next();

        return it;
    }
}
