package com.noleme.commons.container;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 20/01/2017
 */
public class IteratorQueue<T> implements Iterator<T>
{
    private Queue<Iterator<T>> queue;

    /**
     *
     */
    public IteratorQueue()
    {
        this.queue = new LinkedList<>();
    }

    /**
     * 
     * @param iterators
     */
    @SafeVarargs
    public IteratorQueue(Iterator<T>... iterators)
    {
        this();
        for (Iterator<T> it : iterators)
            this.queue.add(it);
    }

    /**
     *
     * @param iterables
     */
    @SafeVarargs
    public IteratorQueue(Iterable<T>... iterables)
    {
        this();
        for (Iterable<T> it : iterables)
            this.queue.add(it.iterator());
    }

    /**
     *
     * @return
     */
    public final IterableQueue<T> asIterable()
    {
        return new IterableQueue<>(this);
    }
    
    @Override
    public final boolean hasNext()
    {
        return this.iterate().hasNext();
    }

    @Override
    public final T next()
    {
        return this.iterate().next();
    }

    @Override
    public final void remove()
    {
        this.current().remove();
    }

    /**
     *
     * @param iterator
     */
    public void add(Iterator<T> iterator)
    {
        this.queue.add(iterator);
    }

    /**
     *
     * @param iterable
     */
    public void add(Iterable<T> iterable)
    {
        this.queue.add(iterable.iterator());
    }

    /**
     *
     */
    private Iterator<T> iterate()
    {
        while (!this.queue.isEmpty() && !this.queue.peek().hasNext())
            this.queue.poll();
        return this.current();
    }

    /**
     *
     * @return
     */
    private Iterator<T> current()
    {
        if (this.queue.isEmpty())
            return (Iterator<T>)Collections.emptyList().iterator();
        return this.queue.peek();
    }

    /*
     * Speed optimization ctors (Iterator)
     */

    /**
     *
     * @param it
     */
    public IteratorQueue(Iterator<T> it)
    {
        this();
        this.queue.add(it);
    }

    /**
     *
     * @param it1
     * @param it2
     */
    public IteratorQueue(Iterator<T> it1, Iterator<T> it2)
    {
        this();
        this.queue.add(it1);
        this.queue.add(it2);
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     */
    public IteratorQueue(Iterator<T> it1, Iterator<T> it2, Iterator<T> it3)
    {
        this();
        this.queue.add(it1);
        this.queue.add(it2);
        this.queue.add(it3);
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     * @param it4
     */
    public IteratorQueue(Iterator<T> it1, Iterator<T> it2, Iterator<T> it3, Iterator<T> it4)
    {
        this();
        this.queue.add(it1);
        this.queue.add(it2);
        this.queue.add(it3);
        this.queue.add(it4);
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     * @param it4
     * @param it5
     */
    public IteratorQueue(Iterator<T> it1, Iterator<T> it2, Iterator<T> it3, Iterator<T> it4, Iterator<T> it5)
    {
        this();
        this.queue.add(it1);
        this.queue.add(it2);
        this.queue.add(it3);
        this.queue.add(it4);
        this.queue.add(it5);
    }

    /*
     * Speed optimization ctors (Iterable)
     */

    /**
     *
     * @param it
     */
    public IteratorQueue(Iterable<T> it)
    {
        this();
        this.queue.add(it.iterator());
    }

    /**
     *
     * @param it1
     * @param it2
     */
    public IteratorQueue(Iterable<T> it1, Iterable<T> it2)
    {
        this();
        this.queue.add(it1.iterator());
        this.queue.add(it2.iterator());
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     */
    public IteratorQueue(Iterable<T> it1, Iterable<T> it2, Iterable<T> it3)
    {
        this();
        this.queue.add(it1.iterator());
        this.queue.add(it2.iterator());
        this.queue.add(it3.iterator());
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     * @param it4
     */
    public IteratorQueue(Iterable<T> it1, Iterable<T> it2, Iterable<T> it3, Iterable<T> it4)
    {
        this();
        this.queue.add(it1.iterator());
        this.queue.add(it2.iterator());
        this.queue.add(it3.iterator());
        this.queue.add(it4.iterator());
    }

    /**
     *
     * @param it1
     * @param it2
     * @param it3
     * @param it4
     * @param it5
     */
    public IteratorQueue(Iterable<T> it1, Iterable<T> it2, Iterable<T> it3, Iterable<T> it4, Iterable<T> it5)
    {
        this();
        this.queue.add(it1.iterator());
        this.queue.add(it2.iterator());
        this.queue.add(it3.iterator());
        this.queue.add(it4.iterator());
        this.queue.add(it5.iterator());
    }
}
