package com.noleme.commons.container;

import java.util.Collection;
import java.util.Iterator;

/**
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public final class Collections
{
    private Collections() {}

    /**
     *
     * @param to
     * @param from
     * @param <T>
     * @return
     */
    public static <T> boolean addAll(Collection<T> to, Iterator<? extends T> from)
    {
        boolean modified = false;

        while (from.hasNext())
        {
            to.add(from.next());
            modified = true;
        }

        return modified;
    }

    /**
     *
     * @param to
     * @param from
     * @param <T>
     * @return
     */
    public static <T> boolean addAll(Collection<T> to, Collection<? extends T> from)
    {
        return addAll(to, from.iterator());
    }
}
