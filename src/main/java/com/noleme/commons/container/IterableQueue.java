package com.noleme.commons.container;

import java.util.Iterator;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 20/01/2017
 */
public class IterableQueue<T> implements Iterable<T>
{
    private IteratorQueue<T> queue;

    /**
     *
     * @param queue
     */
    public IterableQueue(IteratorQueue<T> queue)
    {
        this.queue = queue;
    }

    @Override
    public Iterator<T> iterator()
    {
        return queue;
    }
}
