package com.noleme.commons.container;

import java.util.*;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 22/08/2019
 */
final public class Lists
{
    private Lists()
    {
        throw new Error("The Lists class is not meant to be instantiated.");
    }

    /**
     *
     * @param values
     * @param <T>
     * @return A modifiable List collection containing the provided values.
     */
    public static <T> List<T> of(T... values)
    {
        return new ArrayList<>(Arrays.asList(values));
    }

    /**
     *
     * @param container
     * @param values
     * @param <L>
     * @param <T>
     * @return The provided List collection with the provided values added.
     */
    public static <L extends List<T>, T> L of(L container, T... values)
    {
        container.addAll(Arrays.asList(values));
        return container;
    }
}
