package com.noleme.commons.container;

/**
 *
 * @param <T>
 * @param <U>
 */
public class Pair<T, U>
{
    public T first;
    public U second;

    /**
     *
     * @param first
     * @param second
     */
    public Pair(T first, U second)
    {
        this.first = first;
        this.second = second;
    }
}
