package com.noleme.commons.container;

import java.io.Serializable;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

/**
 * Adaptation of Guava's EvictingQueue, without the ties to other Guava components.
 *
 * @author Pierre LECERF (pierre@noleme.com)
 * Created on 21/12/2019
 */
public final class EvictingQueue<T> implements Queue<T>, Serializable
{
    private final Queue<T> delegate;
    private final int maxSize;

    /**
     * Creates and returns a new evicting queue that will hold up to {@code maxSize} elements.
     *
     * <p>When {@code maxSize} is zero, elements will be evicted immediately after being added to the
     * queue.
     *
     * @param maxSize
     */
    public EvictingQueue(int maxSize)
    {
        if (maxSize < 0)
            throw new IllegalArgumentException("The maxSize argument is expected to be >= 0");

        this.delegate = new ArrayDeque<>(maxSize);
        this.maxSize = maxSize;
    }

    /**
     * Returns the number of additional elements that this queue can accept without evicting; zero if
     * the queue is currently full.
     */
    public int remainingCapacity()
    {
        return this.maxSize - size();
    }

    protected Queue<T> delegate()
    {
        return this.delegate;
    }

    /**
     * Adds the given element to this queue. If the queue is currently full, the element at the head
     * of the queue is evicted to make room.
     *
     * @return {@code true} always
     */
    @Override
    public boolean offer(T e)
    {
        return this.add(e);
    }

    @Override
    public T remove()
    {
        return this.delegate().remove();
    }

    @Override
    public T poll()
    {
        return this.delegate().poll();
    }

    @Override
    public T element()
    {
        return this.delegate().poll();
    }

    @Override
    public T peek()
    {
        return this.delegate().peek();
    }

    /**
     * Adds the given element to this queue. If the queue is currently full, the element at the head
     * of the queue is evicted to make room.
     *
     * @return {@code true} always
     */
    @Override
    public boolean add(T e)
    {
        if (e == null)
            throw new NullPointerException("The element provided to EvictingQueue.add is null");

        if (this.maxSize == 0)
            return true;
        if (this.size() == this.maxSize)
            this.delegate().remove();

        this.delegate().add(e);

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection)
    {
        int size = collection.size();
        if (size >= this.maxSize)
        {
            this.clear();
            return Collections.addAll(this, Iterators.skip(collection, size - this.maxSize));
        }
        return Collections.addAll(this, collection);
    }

    @Override
    public boolean removeAll(Collection<?> c)
    {
        return this.delegate().removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c)
    {
        return this.delegate().retainAll(c);
    }

    @Override
    public void clear()
    {
        this.delegate().clear();
    }

    @Override
    public int size()
    {
        return this.delegate().size();
    }

    @Override
    public boolean isEmpty()
    {
        return this.delegate().isEmpty();
    }

    @Override
    public boolean contains(Object object)
    {
        if (object == null)
            throw new NullPointerException("The element provided to EvictingQueue.contains is null");

        return this.delegate().contains(object);
    }

    @Override
    public Iterator<T> iterator()
    {
        return this.delegate().iterator();
    }

    @Override
    public Object[] toArray()
    {
        return this.delegate().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a)
    {
        return this.delegate().toArray(a);
    }

    @Override
    public boolean remove(Object object)
    {
        if (object == null)
            throw new NullPointerException("The element provided to EvictingQueue.remove is null");

        return this.delegate().remove(object);
    }

    @Override
    public boolean containsAll(Collection<?> c)
    {
        return this.delegate().containsAll(c);
    }

    private static final long serialVersionUID = 0L;
}