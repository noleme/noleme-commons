package com.noleme.commons.container;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 01/08/2019
 */
public class Triplet<T, U, V>
{
    public T first;
    public U second;
    public V third;

    /**
     *
     * @param first
     * @param second
     * @param third
     */
    public Triplet(T first, U second, V third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}
