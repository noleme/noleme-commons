package com.noleme.commons.container;

/**
 * @author Pierre Lecerf (pierre@noleme.com)
 * Created on 01/08/2019
 */
public class Quadruplet<T, U, V, W>
{
    public T first;
    public U second;
    public V third;
    public W fourth;

    /**
     *
     * @param first
     * @param second
     * @param third
     * @param fourth
     */
    public Quadruplet(T first, U second, V third, W fourth)
    {
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }
}
