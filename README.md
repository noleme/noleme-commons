# Noleme Commons

_Last updated for v0.17_

This library is meant as a collection of utility classes, containers and functions, which provide a simple solution to very common use-cases.

Implementations found in this package will obviously be opinionated but shouldn't be tied to any specific Noleme project.

_Note: This library is considered as "in beta" and as such significant API changes may occur without prior warning._

## I. Installation

Add the following in your `pom.xml`:

```xml
<dependency>
    <groupId>com.noleme</groupId>
    <artifactId>noleme-commons</artifactId>
    <version>0.17</version>
</dependency>
```

## II. Notes on Structure and Design

_TODO_

## III. Usage

_TODO_

## IV. Dev Installation

### A. Pre-requisites

This project will require you to have the following:

* Git (versioning)
* Maven (dependency resolving, publishing and packaging) 

### B. Setup

_TODO_
